
<?php
function filebrowser_demo(){

  $form["subfolder"] = array(
  	'#type' => 'textfield',
  	'#title' => t("Subdir"),
  	'#autocomplete_path' 	 	 => 'filebrowser/autocomplete',
 	);

	$cols = array(
		'expander' => 'AJAX callback trigger to expand directories',
		'name'     => 'linked filename, directorys linked to callback (sortable)',
		'file'     => 'linked filename, directories NOT linked (use when embedding)',
		'icon'     => 'linked icon',
		'age'      => 'formatted duration (sortable)',
		'size'     => 'formatted size (sortable)',
		'type'     => 'suffix (sortable)',
		'info'     => 'from the description file',
	);
  $form['cols'] = array(
		'#type' => 'checkboxes', 
  	'#title' => t('Columns to show'), 
  	'#options' => $cols,
  );

	$contexts = array(
		url('filebrowser')          => 'Original filebrowser. Sortable Table',
		url('filebrowser_lite')     => 'Minimal file list',
    url('filebrowser_raw')      => 'Table without surrounding Drupal page, useable by Ajax or RPCs',
		url('filebrowser_list')     => 'Present files in formatted list',
		url('filebrowser_list_raw') => 'List without surrounding Drupal page',
	);
  $form['context'] = array(
		'#type' => 'radios', 
  	'#title' => t('Flavour of filebrowser page'), 
  	'#options' => $contexts,
    '#default_value' => 'filebrowser',
  );

  $form['recursive'] = array(
		'#type' => 'checkbox', 
  	'#title' => t('recursive'), 
  	'#return_value' => 'recursive',
  );

  $form['go'] = array(
		'#type' => 'submit', 
  	'#value' => t('Go'), 
    '#attributes' => array(
      'onclick' => "var form = this.form; for (var i=0; i < form.elements.length; i++) { var e = form.elements[i];if(e.name=='edit[context]') if(e.checked) {form.action = e.value + '/' + form.elements['edit[subfolder]'].value }}"
    )
  );

  $form['#type'] = 'fieldset';
  $form['#title'] = 'Demo of different ways to invoke filebrowser';
  $form = array(
    '#method' => 'GET',
    '#id' => 'demoform',
    'group' => $form,
  );

 	return (drupal_get_form('demo',$form));
}
?>
