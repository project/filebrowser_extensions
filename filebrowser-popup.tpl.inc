<!-- gimme quirks -->
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <!-- 
      don't want to display docs inline in this window 
      All other links should be getting handled by AJAX  
    -->
    <base target="_blank">
    <title>
      Filebrowser - <?= $subfolder ?>
      </title>
      <?php 
      $mod_path = drupal_get_path('module', 'filebrowser_extensions');
      drupal_add_js($mod_path.'/filebrowser.js'); 
      theme_add_style($mod_path .'/filebrowser-popup.css');
      theme_add_style($mod_path .'/filebrowser-tree.css');
      ?>
     <?php print drupal_get_html_head() ?>
     <?php print theme_get_styles() . $styles ?>
<!--[if IE]> 
  <style type="text/css">
    body { height: 100%; overflow: hidden; font-size: 100%; padding-bottom:20px; } 
    div#content { width: 100%; height: 100%; overflow: auto; padding-bottom:60px; padding-top:4px } 
    div#footer { position: absolute; } 
  </style>
<![endif]-->

<script type='text/javascript'>
//<![CDATA[
    function selectPath(selection) {
        if(!window.opener){return;}
        elm = window.opener.document.getElementById('<?= $_GET['targetelement'] ?>');
        if (elm) {
            var subsection = '<?= $_GET['subsection'] ?>';

            elm.value = selection;

            var callback = '<?= $_GET['callback'] ?>';
            if(callback){
              opener.eval(callback+"('"+selection+"')");
            }
            
            elm = $('remainOpen');
            if (elm && elm.checked) return;
            window.close();
            return;
        }
        alert('Unable to set selection <?= $_GET['targetelement'] ?>. Contact with the launch window may have been lost.');
    }

    function setCookie(name, value)
    {
        document.cookie = name+'='+escape(value);
    }

//]]>
</script>

  </head>
  <body id="popup">
      <h1>
        File Browser
      </h1>
    <div id="content" class='filebrowser'>
      <?php
        echo theme_status_messages();
      ?>
      <div id='filebrowser'>      
      <?php
        print $output;
      ?>
      </div>
    </div>
    <div id="footer">
    <div id="controls">
      <span title="When checked the window will not be closed when an item is selected.">
      <input type="checkbox" id="remainOpen" onclick="setCookie('remainOpen', this.checked);" <?= $_COOKIE["remainOpen"]=="true"?"checked=\"checked\"":"" ?> />
      <label for="remainOpen">Remain open</label>
      </span>
    </div>
    </div>
    
  </body>
</html>
