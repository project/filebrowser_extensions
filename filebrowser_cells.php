<?php

/*
 * Created on 15/09/2006
 *
 * All these filebrowser_get_cell_hook() functions return a small array suitable
 * for using as a table TD - containing at minimum a 'data' field. Also possibly
 * sort value column indicators.
 */

/**
 * Callback to render just one cell of data - the name link
 * A filebrowser_get_cell_hook()
 */
function filebrowser_get_cell_name($completepath, $file_info = NULL) {
  if (!$completepath) {
    return array (
      'data' => t("Name"),
      'field' => 'name',
      'description' => t("A link to the short file name. Links to folders open new requests, so don't use this with the AJAX methods")
    );
  }
  $label = $file_info['#title'] ? $file_info['#title'] : basename($completepath);
  if (is_dir($completepath)) {
    // for this link I need to prefix the filebrowser relink, BUT chop the filebrowser directory part
    $href = filebrowser_url($file_info['#rel_path']);
  } else {
    $href = filebrowser_file_url($file_info['#rel_path']);
  }

  $link = l($label, $href);
  return array (
    'data' => $link,
    'class' => 'filename',
    'sv' => $file
  );
}

/**
 * Return the formatted size of this file.
 * A filebrowser_get_cell_hook()
 */
function filebrowser_get_cell_size($completepath, $file_info = NULL) {
  if (!$completepath) {
    return array (
      'data' => t("Size"),
      'field' => 'size', 
      'description' => t("The size of the file, formatted into human-readable Kb,Mb.")
    );
  }
  if (is_dir($completepath)) {
    return array ();
  }
  $size = filesize($completepath);
  return array (
    'data' => $size ? format_size($size) : '',
    'class' => 'size',
    'sv' => ($size ? $size : 0)
  );
}

/**
 * Return the formatted age of this file.
 * A filebrowser_get_cell_hook()
 */
function filebrowser_get_cell_age($completepath, $file_info = NULL) {
  if (!$completepath) {
    return array (
      'data' => t("Age"), 
      'field' => 'age', 
      'description' => t("The age of the file since last modification. Human-readable.")
    );
  }
  $age = time() - filemtime($completepath);
  return array (
    'class' => 'age',
    'sv' => $age,
    'data' => format_interval($age),
  );
}
function filebrowser_get_cell_date($completepath) {
  if (!$completepath) {
    return array (
      'data' => t("Modified"), 
      'field' => 'age',
      'description' => t("The date of the last modification. Human-readable.")
    );
  }
  return array (
    'data' => format_date(filemtime($completepath)),
    'class' => 'date',
    'sv' => filemtime($completepath),
  );
}

/**
 * Return the deduced description or extended info for this file.
 * A filebrowser_get_cell_hook()
 */
function filebrowser_get_cell_info($completepath, $file_info = NULL) {
  if (!$completepath) {
    return array (
      'data' => t("Info"),
      'description' => t("metadata or description of this file as found in nearby info files")
    );
  }
  static $info;

  if (!isset ($info)) {
    // The first time this cell is requested, look hard into the current directory
    // cache results
    $subfolder = (is_dir($completepath)) ? $completepath : dirname($completepath);
    $infopath = $subfolder . '/descript.ion';
    if (!file_exists($infopath)) {
      $infopath = $subfolder . '/files.bbs';
    }

    if(is_file($infopath)){
      $info = filebrowser_extensions_get_fileinfo($infopath, $subfolder);
    } else {
      $info = array ();
    }
  }

  $file = basename($completepath);
  // each info returned was an array. - not sure how to deal with that in one cell
  $detail = (is_array($info[$file])) ? array_pop($info[$file]) : $info[$file];
  return array (
    'data' => $detail,
    'class' => 'info'
  );
}

/**
 * Loads file metainformation from the specified file. Also
 * allows the file to specify a *callback* with which the
 * descriptions are parsed, so more metainformation can be
 * presented on the output.
 * 
 * TODO Actually use this. This func is brought over from 
 * filebrowser_extensions_get_fileinfo() for legacy reasons, but hasn't yet
 * been used in practice
 */
function filebrowser_extensions_get_fileinfo($infopath = NULL, $subfolder = '') {
  static $metacols = array();
  // Return (previously generated) meta column list
  if (!isset ($infopath)) {
    return $metacols;
  }

  // Build meta information list
  $metainfo = array ();
  if (is_readable($infopath) && ($file = file($infopath))) {
    foreach ($file as $line) {
      // Skip empty and commented lines 
      if (trim($line) == '' || strpos(trim($line), '#') === 0) {
        continue;
      }
      list ($name, $description) = explode(" ", $line, 2);
      if (isset ($metainfo[$name])) {
        $metainfo[$name] .= trim($description) . " ";
      } else {
        $metainfo[$name] = trim($description) . " ";
      }
    }

    $callback = FALSE;
    if (isset ($metainfo['*callback*']) && function_exists(trim($metainfo['*callback*']))) {
      $callback = trim($metainfo['*callback*']);
      unset ($metainfo['*callback*']);
    }
    if (isset ($metainfo['.'])) {
      filebrowser_help(NULL, $metainfo['.']);
      unset ($metainfo['.']);
    }

    foreach ($metainfo as $name => $description) {
      $metainfo[$name] = ($callback ? $callback (trim($description), $subfolder, $name) : array (
        trim($description
      )));
    }
    $metacols = ($callback ? $callback () : array (t('Description')));
  }
  return $metainfo;
}

/**
 * @param $filepath
 * @return a table cell
 */
function filebrowser_get_cell_icon($completepath, $file_info = NULL) {
  if (!$completepath) {
    // return the column header
    $cell = array (
      'field' => 'type',
      'description' => t("An icon representing the file type")
    );
  }

  $parts = pathinfo($completepath);
  $extension = $parts['extension'];

  $cell = array (
    'data' => filebrowser_get_icon($completepath), 
    'sv' => $extension
  );

  return $cell;
}

/**
 * Returns the appropriate HTML code for an icon representing
 * a file, based on the extension of the file. A specific icon
 * can also be requested with the second parameter.
 */
function filebrowser_get_icon($completepath = NULL, $iconname = NULL) {
  if (isset ($completepath)) {
    $parts = pathinfo($completepath);
    $iconname = (is_dir($completepath) ? 'folder' : $parts['extension']);
  }
  elseif (!isset ($iconname)) {
    $iconname = 'default';
  }

  $iconfiles = array (
    variable_get('filebrowser_icons','') . "/file-$iconname.png", variable_get('filebrowser_icons', '') . "/file-default.png"
  );
  foreach ($iconfiles as $icon) {
    if (file_exists($icon)) {
      return theme("image", $icon, $iconname . " file", $iconname . " file");
    }
  }
  return '';
}

/**
 * Return a cell linked to the file. 
 * Like get_cell_name, but do NOT link directories or provide uplink.
 * Clicking on a directory here will expand it, via js, not browse into it. This
 * version includes the icon inline, for easier linking and layout.
 * 
 * A filebrowser_get_cell_hook()
 */
function filebrowser_get_cell_file($completepath, $file_info = NULL) {
  if (!$completepath) {
    return array (
      'data' => t("Name"), 
      'field' => 'name', 
      'description' => t("The icon and file name together. Directories are not linked, but have classes embedded to make this useful for AJAX callbacks. Files are linked to launch in a new window.")
    );
  }

  $file = basename($completepath);
  $rel_path = $file_info['#rel_path'];

  if ($file == "..") { //return; }
    // construct uplink
    $cell = array (
      'data' => "<a href='" . filebrowser_expander_link(dirname(dirname($rel_path))) . "'></a> " . filebrowser_get_icon($completepath) . " up$file", 
      'class' => "fileBrowserUp filename", 
      'id' => filebrowser_id($rel_path . '_file'),
    );
    return $cell;
  }

  if (is_dir($completepath)) {
    $cell = array (
      'data' => "<a href='" . filebrowser_expander_link($rel_path) . "'></a> " . filebrowser_get_icon($completepath) . " $file", 
      'class' => "fileBrowserToggle filename", 
      'id' => filebrowser_id($rel_path . '_file'),
    );
    return $cell;
  }
  return array (
    'data' => l(filebrowser_get_icon($rel_path) . ' ' . $file, filebrowser_file_url($rel_path), NULL, NULL, NULL, NULL, TRUE), 
    'class' => 'filename', 
    'sv' => $file
  );
}

/**
 * A sample filebrowser column callback.
 * 
 * This trivial function is mainly here as a demo to copy if you want to develop
 * your own cells.
 * 
 * A filebrowser_get_cell_hook()
 * @param $filepath
 * @return a table cell
 */
function filebrowser_get_cell_type($completepath, $file_info = NULL) {
  if (!$completepath) {
    // return the column header
    $cell = array (
      'data' => t("Type"), 
      'field' => 'type', 
      'description' => t("The file suffix. Sortable.")
    );
  }

  // Return a cell displaying the suffix. Sortable.
  $parts = pathinfo($completepath);
  $extension = $parts['extension'];
  if ($extension) {
    $cell = array (
      'data' => $extension,
      'sv' => $extension
    );
  }

  return $cell;
}

/**
 * Add an ajax expando to the given row.
 * A filebrowser_get_cell_hook()
 */
function filebrowser_get_cell_expander($completepath, $file_info = NULL) {
  // Add ajax functions to file browser (once only)
  static $done_lib;
  if (!$done_lib) {
    drupal_add_js('misc/progress.js');
    drupal_add_js(drupal_get_path('module', 'filebrowser_extensions') . '/filebrowser.js');
    $done_lib = TRUE;
  }
  if (!$completepath) {
    return array (
      'description' => t("An ajax expando. Click to expand tree")
    );
  }

  $rel_path = $file_info['#rel_path'];

  if (basename($rel_path) == "..") {
    $cell = array (
      'data' => "<a href='" . filebrowser_expander_link(dirname(dirname($rel_path))) . "'></a><acronym class='expando'></acronym>", 
      'class' => "fileBrowserUp", 
      'id' => filebrowser_id($rel_path),
    );
    return $cell;
  }

  if (is_dir($completepath)) {
    // Return a cell displaying the trigger.
    $cell = array (
      'data' => "<a href='" . filebrowser_expander_link($rel_path) . "'></a><acronym class='expando'>*</acronym>",
      'class' => "expander fileBrowserExpand", 
      'id' => filebrowser_id($rel_path),
    );
    // the URL gets a randomizer on the end to prevent strange xmlhttp stalls that seem to be cache related
  } else {
    // need something to keep css from collapsing.
    $cell = array (
      'class' => "expander",
    );
  }

  return $cell;
}

/**
 * Returns a link that contains the details needed for th AJAX callback to work
 * on a directory expander link.
 */
function filebrowser_expander_link($rel_path) {
  $profile = filebrowser_profile();
  return url($profile['path'] . '/' . $rel_path, 'mode=raw');
}

/**
 * Callback used by filebrowser to annotate the filebrowser lists with my extra
 * action. 
 * A filebrowser_get_cell_hook()
 * @param $filepath the full filesystem path
 */
function filebrowser_get_cell_select_button($completepath, $file_info = NULL) {
  if (!$completepath) {
    // return the column header (blank)
    return array (
      'data' => 'Choose',
      'description' => t("A button that fires a JS function selectPath(rel_path)."
    ));
  }

  $cell = "<span 
       onclick=\"if(typeof(selectPath)=='function') selectPath('" . addslashes($file_info['#rel_path']) . "')\" 
       title=\"Select this file\" class=\"filecontrols\" 
      > select </span>";
  return array (
    'data' => $cell,
    'class' => 'action'
  );
}

/**
 * Callback used by filebrowser to annotate the filebrowser lists
 * A filebrowser_get_cell_hook()
 * Although it looks like a cell, it doesn't return a 'data' value, so won't
 * work out of context of a formtable. The 'form' element this cell contains
 * must be rendered using formAPI
 */
function filebrowser_get_cell_checkbox($completepath, $file_info = NULL) {
  // Add ui functions to file browser (once only)
  static $done_lib;
  if (!$done_lib) {
    drupal_add_js(drupal_get_path('module', 'filebrowser_extensions') . '/toggle_tree.js');
    $done_lib = TRUE;
  }

  if (!$completepath) {
    // return the column header (blank)
    return array (
      'description' => t("A checkbox returning an edit[filepath] parameter to the form this table is embedded in."
    ));
  }

  $rel_path = $file_info['#rel_path'];

  $cell = array (
    $rel_path => array (
      '#type' => 'checkbox',
    )
  );

  // Using form API in this context was too hard
  $cell = "\n<input type='checkbox' name='edit[filepath][]' value='$rel_path' id='checkbox_" . filebrowser_id($rel_path) . "' class='filebrowser_checkbox' />";
  return array (
    'data' => $cell,
    'class' => 'checkbox'
  );
}

function filebrowser_get_cell_filename($rel_path, $file_info) {
  return $file_info['#filename'];
}