
// Declare big lookup globally to avoid doing them too often

var all_inputs = document.getElementsByTagName('input');

/**
 * Attaches the toggletree behaviour to container checkboxes.
 */
function toggleTreeAutoAttach() {
  for (var c = 0; c < all_inputs.length; c++)
  {
    var input_node = all_inputs[c];
    if(! hasClass(input_node, 'filebrowser_checkbox')) continue;
    
    // the checkbox may not know if it's a container, that's a property of the row
    var td = input_node.parentNode;
    var tr = td.parentNode;
    tr.onclick = toggle_row;
    
    // disable normal activity, row clicking handles it now
    input_node.onclick = function() {this.checked = ! this.checked; }
    input_node.onchange = enhance_container;
		input_node.onchange(); // Do it now to reflect any browser-cached form status
		
    if (hasClass(tr, 'container')) {
      input_node.onchange = toggle_tree_children;
    }
  }
}

if (isJsEnabled()) {
  addLoadEvent(toggleTreeAutoAttach);
}

/**
 * Handler for a container checkbox onclick
 * Every box turn on!
 */
function toggle_tree_children(e){
  var id = this.id; 
  if(typeof(setCheckboxes) == 'function') // setCheckboxes may be supplied by tweakUI library. If not, no biggie.
		setCheckboxes( id+"_contents", this.checked ); // used for the table structure - associated element, not the current one.
	this.enhance = enhance_container; this.enhance();

	if(window.event) window.event.cancelBubble = true;
  if(e){e.stopPropagation();}
  return false
}
        
/*
 * As well as just toggling the checkbox, toggle the css class of this boxes label
 * For great brightness!
 * Should work on either table TRs or list LIs
 */
function toggle_row(e) {
  var checkbox = this.getElementsByTagName( 'input' )[0];
	var turn_on = (! checkbox.checked);

  checkbox.setAttribute('checked',turn_on); // dom/ie
  checkbox.checked = turn_on; // dhtml/moz
	if(checkbox.onchange)checkbox.onchange();

	if(window.event) window.event.cancelBubble = true;
  if(e){e.stopPropagation();}
}

function enhance_container(){
	var p = this.parentNode;
	while(p && (p.nodeName != 'TR') && (p.nodeName != 'LI')) p = p.parentNode;
	if(p){enhance_row_visually(p)}
}

function enhance_row_visually(row){
  var checkbox = row.getElementsByTagName( 'input' )[0];
  if(!checkbox){return};

	if (checkbox.checked  ) {
	  addClass(row, 'marked');
	} else {
		removeClass(row, 'marked');
	}
}
