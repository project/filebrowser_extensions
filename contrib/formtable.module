<?php

/**
 * @file
 * An enhancement to the 4.7 forms API that treats table building
 * in the same way as form building. Meaning we can build tabular forms.
 */

/**
 * Implementation of hook_help().
 */
function formtable_help($section) {
  switch ($section) {
    case 'admin/modules#description':
      return t('Build tables using the Forms API. <em>A utility extension library</em>');
      break;
    case 'admin/help#formtable' :
      return formtable_help_page();
  }
}

function formtable_help_page(){

$output = "
<h2>The object: to use the forms API to render tabular data</h2>
<p>
This can probably be done by extending the form builder functions
and inserting table structure where currently we have fieldset structure.
</p>
<p>Table layouts are not all bad. 
Sometimes, when dealing with actual data, a table is the right thing.
Could phpadmin work without tables?
Anyway, my data structure needs tables. And form elements inside them.
</p>
<h3>An example form/table structure</h3>
The form table I want (built as HTML string):
";

$table = "
<table border='2'>
  <tr>
    <td colspan='2'><input value='Dewey'/></td>
  </tr>
  <tr>
    <td><input value='Huey'/></td>
    <td><input value='Louie'/></td>
  </tr>
</table>
";

$output .= $table;
$output .= "
... which could trivially be built like this:
<pre>".htmlspecialchars($table)."</pre>

<h3>Attempting the same in forms API however...</h3>
Well, First I'll establish the heirachy, a bit like

<pre>
  form:
    fieldset:
      field: 'Dewey'
    fieldset:
      field: 'Huey'
      field: 'Louie'
</pre>

And once that's taking shape, I need to fill in the Forms API verbose definitions.

<h2>Basic, Structured Form</h2>
It could end up looking like:
";

$dewey_field = array(
  '#type' => 'textfield', 
  '#default_value' => 'Dewey',
);
$huey_field = $dewey_field; 
$huey_field['#default_value']  = 'Huey';
$louie_field = $dewey_field; 
$louie_field['#default_value'] = 'Louie';

$group = array();
$group['row1'] = array(
  '#type' => 'fieldset', 
  '#title' => t('row 1'), 
);
$group['row1']['huey'] = $huey_field;

$group['row2'] = array(
  '#type' => 'fieldset', 
  '#title' => t('row 2'), 
);
$group['row2']['dewey'] = $dewey_field;
$group['row2']['louie'] = $louie_field;

$group['submit_button'] = array(
  '#type' => 'button', 
  '#value' => 'Submit', 
);

$form = array( 'group' => $group );

$output .= "<pre><small>".print_r($form,1)."</small></pre>";


// save a copy before processing
$form2 = $form;
$got_form = drupal_get_form('form_id', $form);

//print('form is initialized');
//print("<small><pre>got form: ".htmlspecialchars(print_r($form,1))."</pre></small>");
$output .= "<p>
  This form definition now gets run through the wringer, 
  and after <code>drupal_get_form()</code> is done with it, it renders like:
</p>";

$output .= "<h3>The Form, rendered normally</h3>";
$output .= $got_form ;


$output .= "
<h2>Now, turn the form into a table</h2>
<p>
Formgroups from my trivial example become trs, fields become tds.
This is simply convenient, in practice the actual nesting may have to 
be thought out... but it's no harder than the old <code>theme_form()</code> function.
</p><p>
I define a few new rendering functions
<code>theme_tr(), theme_td()</code>
for the formbuilder to find.
Publish these with a <code>hook_elements()</code> implimentation to declare their existance and how I expect them to behave.
</p><p>
Add some logic to allow lazy definitions 
(
  any child of a TABLE is expected to be a TR, 
  any child of a TR must be a TD
)
... using a '#process' parameter.
This process is a shortcut, but it assumes that anonymous arrays should implicitly have an appropriate
'#type' set, and that elements that don't fit the rules of table structure will be shuffled down until they find a td to fall into.
</p>
<p>I'll modify the form structure I began with to get:
<pre>
  table:
    tr:
      td:
        field: 'Dewey'
    tr:
      td:
        field: 'Huey'
      td:
        field: 'Louie'
</pre>
</p>
";

$form2['group']['#type'] = 'formtable';
$form2['group']['#attributes'] = array('border'=>'2');

// turn formgroups into trs, 
// the items within them will get wrapped in tds
$form2['group']['row1']['#type'] = 'tr';
$form2['group']['row2']['#type'] = 'tr';

$output .= "It now looks like:<small><pre>".htmlspecialchars(print_r($form2,1))."</pre></small>
";
$output .= "<h2>Result! A formtable</h2>";

// re-initialize
$got_form = drupal_get_form('form_2', $form2);

//$output .= "<small><pre>".htmlspecialchars(print_r($form2,1))."</pre></small>";
$output .= "
Setting <code>drupal_get_form()</code> on this new structure now gives me:
<small><pre>".htmlspecialchars(print_r($got_form,1))."</pre></small>
<h3>The Form as a table</h3>
$got_form
<p>
And with some work, the rest of the forms arsenal can be squeezed into this page-building!
Dunno how far I can drag this towards the table-builder with sorting columns and all.
I don't think I need that yet..
</p>
<p>
Note how the button element (that was simply defined as a top-level member of the form)
has ended up in its own cell. This could be constructed neater if you took the time,
but I'm letting default behaviour format it for now.
</p>
<p>
I have some tweaking to do to enforce XHTML compliance (TBODY, maybe TH and that crap) but it looks trivial.
TD attributes like colspan can be added in the same way as the old theme_table() did and the new forms API does.
</p>
";

return $output;
  
}



/**
 * Build an HTML tag. 
 * Very generic.
 * Uses #type, #attributes, #children and #value
 */
function theme_element($element){
  return '<'.$element['#type'] . drupal_attributes($element['#attributes']) .' >' . $element['#children'] . $element['#value'] . '</'.$element['#type'].">\n";
}

function theme_formtable($element){
  return "\n".'<table' . drupal_attributes($element['#attributes']) .' ><tbody>' . $element['#children'] . $element['#value'] . "</tbody></table>\n";
}
function theme_tr($element){
  return theme_element($element);
}
function theme_td($element){
  return theme_element($element);
}
function theme_th($element){
  return theme_element($element);
}

/**
 * return custom fields for my new element definitions
 */
function formtable_elements(){
  $type['td'] = array('#process' => array('ensure_text' => array()), '#tree' => TRUE );
  $type['th'] = array('#process' => array('ensure_text' => array()), '#tree' => TRUE );
  $type['tr'] = array('#process' => array('ensure_table_cells' => array()), '#tree' => TRUE );
  $type['formtable'] = array('#process' => array('ensure_table_rows' => array()), '#tree' => TRUE );
  return $type;  
}

/**
 * Given a TR element, the only valid children are TDs
 * If the child is NOT a TD, wrap it up as if it were one
 */
function ensure_table_cells($element){
  // should I use element_children($element) ?
  foreach($element as $key=>$cell){
    if($key[0] == '#'){continue;}
    if($cell['#type'] != 'td'){
      if(is_string($cell)){
        $element[$key] = array('#type' => 'td', '#value' => $cell );
      } else {
        $element[$key] = array('#type' => 'td', $cell );
      }
    }
  }
  return $element;
}

/**  
 * cast random plaintext values encountered in the structure
 * into text elements
 */
function ensure_text($element){
  foreach($element as $key=>$cell){
    if($key[0] == '#'){continue;}
    if(is_string($cell)){
      $element[$key] = array('#value' => $cell );
      debug("plaintext '$cell' found in the middle of a forms API structure. Casting it into a text node. This should be avoided",E_USER_WARNING );
    }
  }
  return $element;
}


function ensure_table_rows($element){
  // should I use element_children($element) ?
  foreach($element as $key=>$cell){
    if(! is_array($cell)){continue;}
    if($key[0] == '#'){continue;}
    if(! in_array($cell['#type'] , array('caption','thead','tbody','tfoot','tr') ) ){
      $element[$key] = array('#type' => 'tr', $cell );
    }
  }
  return $element;
}

