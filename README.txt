
Drupal filebrowser_extensions.module README.txt
===============================================

Filebrowser extensions is a rewrite of the Drupal filebrowser module,
now totally stand-alone. It provides directory browsing functionality,
allowing direct, enhanced access to public folders similar to FTP browsing
within a Drupal site. 
In addition, it allows :
- in-page file listings, 
- Ajax directory tree explorer,
- autocomplete and pop-up widgets,
- list (as well as table) rendering,
- and custom info columns for any type of metadata

 +--------------------------------------------------------+
 | IMPORTANT. If you receive "An HTTP error 404 occured"  |
 | alert during autocompletes, this is an Apache problem. |
 | See the bugfix at the end.                             |
 +--------------------------------------------------------+

Accessing filebrowser
---------------------------------------
For security and configurability, Filebrowsing is now presented in many
individual profiles.

A filebrowser profile is a configuration that specifies what directories can be
seen by whom, and how they are presented.

There are several different parameters that can be set for each filebrowser
profile or 'view'. Available filebrowser directories can be independant of
the global file_directory_path, different users and uses mean that the
browseable area may be rooted at (restricted to) subfolders within that, and
at any point, the user may be browsing to a lower level still. To protect the
internals from casual browsers, only the last part of the path will be
displayed on the URL, the full paths that get prepended to it in practice
must be deduced via context (the path that's being called) and is configured
by admins only.

To 'publish' a directory you would create a new profile in the 
admin/settings/filebrowser_extensions , 
giving it a new <b>path</b> it will become available as.
Set parameters, choose rendering options, 
and link to or make a menu item for this area.



When using the Ajax expanders, the child folders use the same
rendering configurations as the parent.
Thus AJAX callbacks are explicitly told what the context is 
(via a 'mode' parameter). This ensures the number and type 
of columns to display, and what rendering engine (table or list) is correct.



Using Filebrowser as a widget in other contexts
---------------------------------------
Inline
---------------------------------------
The filebrowser renderer can be used embedded in other pages, or stand-alone.

To EMBED a filebrowser table, the following PHP snippet could be copied into 
a (php code) node:

	<?php
	print filebrowser_embed('images');
	?>

The default filebrowser context is the system 'files/' directory, and this 
snippet renders a listing of the contents of 'files/images'.

This is useful for providing direct access to a download directory, but is only 
the default rendering, and doesn't behave with subdirectories.

A slightly more advanced version lets you select which columns to show. 

<?php
  $folder = 'images';
  
  $profile = array(
  	'cols'   => 'icon;name;size',// also used as headers. (no tablesort)
  	'render' => 'table',
  );

	print filebrowser_embed($folder,$profile);

?>

Handling deep browsing to folders is trickier, and best done as a custom 
callback, not a PHP snippet, but the AJAX expanders can be told to work. 
A contextual profile will be created and then remembered for later
(asynchonous) requests.

<?php
  $folder = '';
  $context = "demo3";
  	// each unique embed profile needs its own 'path' or context
  	// in order to serve up the same configurations on the later 
  	// ajax calls.
  
  $profile = array(
  	// very simple list
  	'cols'   => 'expander;file',
  	'render' => 'list',
  	'path'   =>  $context, 
  );
  
	// scan & render
	print filebrowser_embed($folder,$profile);
?>

The default table/column layout does not work wonderfully with multiple nesting 
levels, and you may find it more aesthetic and sematic to use list elements 
instead.

<?php
  $folder = '';
  $context = "demo4";

  $profile = array(
	  'cols' => array('expander','file'),
  	'path' =>  $context, 
  );
  
	print filebrowser_embed($folder,$profile);
?>
Again, we needed to create the dummy demo context in order to support 
the later AJAX calls.


---------------------------------------
Custom columns
---------------------------------------
Available columns are :
expander : AJAX callback trigger to expand directories
name     : linked filename, directorys linked to callback (sortable)
file     : linked filename, directories NOT linked (use when embedding)
icon     : linked icon
age      : formatted duration (sortable)
size     : formatted size (sortable)
type     : suffix (sortable)
info     : description, extracted as described above

More can be added by custom modules! Then invoked from other contexts.
See the filebrowser_get_cell_hook() examples in the code.

---------------------------------------
Popup
---------------------------------------
Filebrowser can now be used as a standalone popup widget.
The following code will launch a small filebrowser window, which will send back
the path of the selected file to the named form element.
<pre>
  <form>
    <input type=text id='fillme'/>
    <input type='button' value='browse files' onclick='goBrowse()' />
    <script>  
      function goBrowse(){
        window.open(
        "<?php echo url("filebrowser_popup" ,"targetelement=fillme"); ?>",
        "popup", 
        "width=400, height=600, resizable=yes"
        )
      }
    </script>
  </form>
</pre>

---------------------------------------
Autocomplete
---------------------------------------
Also, an autocomplete handler is available. Creating a form element:
<?php
   $form["choose_file"] = array(
  	'#type'              => 'textfield',
  	'#title'             => t("File"),
  	'#autocomplete_path' => 'filebrowser_autocomplete',
 	);

 	print(drupal_get_form('demo',$form));
?>
 ... will help you fill in the gaps.

Extending the	'#autocomplete_path' , eg:
'filebrowser_autocomplete/avatars' 
would start the completion from below that directory.
(browsing is always limited to below the system 'files' directory, so paths 
begin relative to that) 

Autocomplete in subdirectories requires a bugfix in form.inc 4.7.2
http://drupal.org/node/52116#comment-116656 
 
For extreme examples, see the attach.module.
