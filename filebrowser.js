/**
 * Expand subdirectories withing filebrowser, using http requests to the server
 * This code is very dependant on the exact HTML of the table it works upon.
 * The trigger should be the first cell, and should contain an anchor referencing
 * the file URL, and an empty tag where the +/- symbol can be displayed
 * 
 * @author dman
 */

if (isJsEnabled()) {
  addLoadEvent(filebrowserAutoAttach);
}

/**
 * Attaches the expand behaviour to the filebrowser row.
 */
function filebrowserAutoAttach() {
  filebrowserAutoAttachTo('span');
  filebrowserAutoAttachTo('td');
}

function filebrowserAutoAttachTo(tagName) {
  var cells = document.getElementsByTagName(tagName);
	if(!cells)return;
  for (var i = 0; cell = cells[i]; i++) {
    if(!cell) continue
		marker = cell.firstChild;
		if(marker)marker = marker.nextSibling;
    if(marker && ! hasClass(marker, 'expando')){ marker = 0 }
      
    if (hasClass(cell, 'fileBrowserExpand')) {
      cell.onclick = expand_filebrowser;
      if(marker){
	      marker.innerHTML = '+';
  	    marker.setAttribute('title','expand');
  	  } // else alert("No Marker ... "+cell.outerHTML)
    }
    if (hasClass(cell, 'fileBrowserContract')) {
      cell.onclick = contract_filebrowser;
      if(marker){
	      marker.innerHTML = '-';
  	    marker.setAttribute('title','contract');
  	  }
    }
    if (hasClass(cell, 'fileBrowserUp')) {
      cell.onclick = shiftup_filebrowser;
      if(marker){
	      marker.innerHTML = '^';
  	    marker.setAttribute('title','up');
  	  }
    }
    if (hasClass(cell, 'fileBrowserToggle')) {
      cell.onclick = toggle_filebrowser;
    }
  }
}


/**
 * Event handler. ID the context, set a progress bar and make the request
 */
function expand_filebrowser(trigger){
  trigger = trigger? trigger : this;
  if(trigger.target){trigger=trigger.target;}
  // work up to the td this click came from. That's the trigger we hinge on.
  while ((trigger.nodeName!='TD')&&(trigger.nodeName!='SPAN')&&(trigger=trigger.parentNode)){;}

  var link = trigger.firstChild;
  var uri = link.getAttribute('href')+'&d='+new Date();
  // date suffix avoids some abberant http request caching

  var row = trigger.parentNode;
  var rowid =  trigger.id + '_row';
  row.id = rowid;
  wrapperid = trigger.id+'_content';

	if($(wrapperid)) contract_filebrowser(trigger); // remove an old one?

  // Create space to put the new stuff into
  // This is done differently for LIs vs TRs
  switch(row.nodeName){
  	case 'LI' :
		  wrapper = document.createElement('div');
  		row.appendChild(wrapper);  
 		break;
  	case 'TR' :
		  tr = document.createElement('tr');
		  tr.setAttribute('id',trigger.id+'_content_row')
		  if(after = row.nextSibling)
		    row.parentNode.insertBefore(tr,after);  
		  else
		    row.parentNode.appendChild(tr);  
		  td = document.createElement('td');
		  tr.appendChild(td);  
		  // layout goes hoopy if I depend too much on accurate colspans in IE
		  wrapper = document.createElement('td');
		  wrapper.setAttribute('colSpan',row.childNodes.length-1);
		  tr.appendChild(wrapper);
		break;
  }

  wrapper.setAttribute('id',wrapperid);

  trigger.progress = add_progress_bar(wrapper);
  wrapper.trigger = trigger;

	// window.open(uri,'new');
  trigger.onclick = null; // prevent double-click
  HTTPGet(uri, display_subdirectory, wrapperid)
}

function add_progress_bar(wrapper){
  wrapper.progress = new progressBar('uploadprogress');
  wrapper.progress.setProgress(-1, '');
  wrapper.progress.element.style.width = '100%';
  wrapper.progress.element.style.height = wrapper.offsetHeight/2 +'px';
  wrapper.style.height = '10px';
  wrapper.appendChild(wrapper.progress.element);
	return wrapper.progress;
}

/**
 * Content has arrived, place it inline
 */
function display_subdirectory(response, handle, wrapperid){
  wrapper = $(wrapperid)
  if(!wrapper){alert("No "+wrapperid+" to put directory listing in.\nSomething went wrong with the formatting.");return;}
  if(wrapper.progress)
	  removeNode(wrapper.progress.element);
  wrapper.progress = null;
	wrapper.style.height = 'auto'; // needed to allow it to expand again
	try{
	  if(response) wrapper.innerHTML = response;
	  else alert("Bad (null) response from request")
	} catch(e) {
		clipboardData.setData("Text", response);
	  alert( "Unable to insert response in the document. The returned HTML may not be valid to put there.\nThe problem HTML was copied to clipboard for reference." );
	}

  if(wrapper.trigger){
    wrapper.trigger.className = 'fileBrowserContract list_expander';
  }

  // Need to do this again to apply events to the new markup
  filebrowserAutoAttach();
  if(typeof(toggleTreeAutoAttach)=='function')toggleTreeAutoAttach();
  return;
}

/**
 * Event handler for clicking '-' - remove the subdir content and toggle action back to 'expand'
 */
function contract_filebrowser(trigger){
  trigger = trigger? trigger : this;
  if(trigger.target){trigger=trigger.target;}
  while ((trigger.nodeName!='TD')&&(trigger.nodeName!='SPAN')&&(trigger=trigger.parentNode)){;}
  var row = trigger.parentNode;

  if(wrapper = $(trigger.id+'_content')) // li version
    wrapper.parentNode.removeChild(wrapper);  

  if(wrapper = $(trigger.id+'_content_row')) // table version
    wrapper.parentNode.removeChild(wrapper);  
    
  trigger.className = 'fileBrowserExpand list_expander';
  filebrowserAutoAttach()
}

function toggle_filebrowser(trigger){
  trigger = trigger? trigger : this;
  if(trigger.target){trigger=trigger.target;}
  // work up to the tr, and down to the first td (in case trigger was launched from another cell)
  while ((trigger.nodeName!='TR')&&(trigger.nodeName!='LI')&&(trigger=trigger.parentNode)){;}
	// now scan the current row to find the right trigger that holds the expando code
	expando = trigger.firstChild;
  while (expando){
  	if (hasClass(expando, 'fileBrowserExpand')) {expand_filebrowser(expando)}
  	else if (hasClass(expando, 'fileBrowserContract')) {contract_filebrowser(expando)}
    expando = expando.nextSibling;
  }
}

/** 
 * Tricky. Replace the container of the trigger with a list of this items parents.
 */
function shiftup_filebrowser(trigger){
  trigger = trigger? trigger : this;
  if(trigger.target){trigger=trigger.target;}
  while ((trigger.nodeName!='TD')&&(trigger=trigger.parentNode)){;}
  var link = trigger.firstChild;
	var uri = link.getAttribute('href')+'&d='+new Date();

	// Now find current containing table
	var container = trigger;
  while ((container.nodeName!='TABLE')&&(container=container.parentNode)){;}
  container=container.parentNode;
  wrapperid = container.getAttribute('id');
  if(! wrapperid){
	  wrapperid = 'up-target'
  	container.setAttribute('id',wrapperid);
  }
  
  p = new progressBar('uploadprogress');
  p.setProgress(-1, '');
  p.element.style.width = '100%';
  container.appendChild(p.element);
  container.progress = p;

  trigger.onclick = null; // prevent double-click
  HTTPGet(uri, display_subdirectory, wrapperid)
}

